from ComplaintDB import ComplaintDB
import matplotlib.pyplot as plt

plt.style.use(['seaborn-darkgrid', 'seaborn-deep'])

comp = ComplaintDB('complaints')
df = comp.daily_update_count('2018-08-01', '2018-08-31')
df, df2 = df.groupby(['date_sent_to_company'])['count'].sum(), df

df2, df3 = (
    df2[df2['company'] == 'BANK OF AMERICA, NATIONAL ASSOCIATION'],
    df2[df2['company'] == 'CAPITAL ONE FINANCIAL CORPORATION'])

print(df.head())
print(df2.head())
print(df3.head())

fig, ax = plt.subplots()
# df.plot(marker='.', ax=ax, markersize=8, legend=False)
# ax.plot(df)
ax.plot(df2.index, df2['count'], label='BANK OF AMERICA, NA')
ax.plot(df3.index, df3['count'], label='CAPITAL ONE')
ax.legend()
ax.set_title('Update count by day')
ax.set_xlabel('Date')
ax.set_ylabel('Number of complaints')
fig.autofmt_xdate()
# ax.xaxis.set_ticks(df.index)
fig.tight_layout()
plt.show()
