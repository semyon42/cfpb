from configparser import ConfigParser
from sqlalchemy import create_engine
from sqlalchemy import types
import os
import sys
import pandas as pd
import numpy as np
import logging

if len(sys.path[0]) > 0:
    os.chdir(sys.path[0])

logger = logging.getLogger('complaint_db')
logger.setLevel(logging.DEBUG)

file_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stream_formatter = logging.Formatter('%(message)s')

file_handler = logging.FileHandler('complaintdb.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(file_formatter)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(stream_formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)


class ComplaintDB:

    def __init__(
            self,
            table_name: str,
            config_file: str = 'db.config',
            chunksize: int = 2000):

        db_config = ConfigParser()
        db_config.read(config_file)
        engine_str = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'
        engine_str = engine_str.format(**db_config['db'])

        # TODO: Unhardcode app token
        self.endpoint = 'https://data.consumerfinance.gov/resource/jhzv-w97w.json?'
        self.endpoint += '$$app_token=bbgxygWKJFcr1xFFyAvpnSIcr'
        self.engine = create_engine(engine_str)
        self.table_name = table_name

        self.chunksize = chunksize

        self.sql_dtypes = {
            'company': types.Text(),
            'company_response': types.Text(),
            'company_public_response': types.Text(),
            'complaint_id': types.BigInteger(),
            'consumer_consent_provided': types.Text(),
            'consumer_disputed': types.Text(),
            'complaint_what_happened': types.Text(),
            'date_received': types.DateTime(),
            'date_sent_to_company': types.DateTime(),
            'issue': types.Text(),
            'product': types.Text(),
            'state': types.Text(),
            'sub_issue': types.Text(),
            'sub_product': types.Text(),
            'submitted_via': types.Text(),
            'tags': types.Text(),
            'timely': types.Text(),
            'zip_code': types.Text(),
            'update_stamp': types.DateTime()
        }

        self.columns = [key for key in self.sql_dtypes.keys()]

        self.logger = logging.getLogger('complaint_db')

    def update_db(self):
        '''
        Load new entries into the database. Track changes.
        '''

        try:
            self.logger.info('Updating database...')

            current_date = pd.Timestamp.today()
            start_date = current_date - pd.DateOffset(months=1)
            start_date = start_date.strftime('%Y-%m-%d')

            # From matching complaint_id's select one with highest update_stamp

            query = f'SELECT DISTINCT ON (complaint_id) * '
            query += f'FROM {self.table_name} '
            query += f'WHERE date_sent_to_company IS NULL OR date_sent_to_company >= \'{start_date}\' '
            query += f'ORDER BY complaint_id, update_stamp DESC'
            query += ';'

            local_df = pd.read_sql_query(query, self.engine)
            local_df.drop(labels='update_stamp', axis=1, inplace=True)
            local_df.set_index('complaint_id', inplace=True)

            # TODO: deal with limit
            request_str = self.endpoint + f'&$where=date_sent_to_company%20>=%20\'{start_date}\'&$limit=100000'

            remote_df = pd.read_json(request_str)
            remote_df.set_index('complaint_id', inplace=True)

            if remote_df.empty:
                self.logger.info('No new entries in the past month')
                return

            left_ids, right_ids = set(local_df.index), set(remote_df.index)

            left_only = left_ids.difference(right_ids)
            right_only = right_ids.difference(left_ids)
            both = left_ids.intersection(right_ids)

            to_write = pd.DataFrame()

            # Deleted entries
            if left_only:
                tmp_local = local_df.loc[local_df.index.isin(left_only)]
                # Not marked as deleted yet
                tmp_local = tmp_local.loc[tmp_local.notnull().any(axis=1)]

                if not tmp_local.empty:
                    self.logger.info(f'Deleted entries with complaint_id: {list(tmp_local.index)}')
                    tmp_local.loc[:, tmp_local.columns] = np.nan
                    to_write = to_write.append(tmp_local, sort=True)

            # Existing entries
            if both:
                tmp_local = local_df[local_df.index.isin(both)].sort_index()
                tmp_remote = remote_df[remote_df.index.isin(both)].sort_index()
                tmp_local.fillna(value='', inplace=True)
                tmp_remote.fillna(value='', inplace=True)
                tmp_remote = tmp_remote.astype(tmp_local.dtypes)
                changed_rows = (tmp_local != tmp_remote).any(axis=1)

                if changed_rows.any():
                    self.logger.info(f'Changed entries with complaint_id: {list(tmp_remote[changed_rows].index)}')
                    to_write = to_write.append(tmp_remote[changed_rows], sort=True)

            # New entries
            if right_only:
                self.logger.info(f'{len(right_only)} new entries')
                tmp_remote = remote_df[remote_df.index.isin(right_only)].sort_index()
                to_write = to_write.append(tmp_remote, sort=True)
            else:
                self.logger.info('No new entries')

            if to_write.empty:
                return

            to_write['update_stamp'] = current_date

            to_write.to_sql(
                self.table_name,
                self.engine,
                if_exists='append',
                dtype=self.sql_dtypes)

        except Exception:
            self.logger.exception('Failed to update database')

        else:
            self.logger.info('Update successful')

    def reload_db(self):
        '''
        Replace the whole table with a snapshot from CFPB.
        !!!Drops the existing table!!!
        '''

        # TODO: Stop update task until complete

        self.logger.info('Loading snapshot into database...')
        current_date = pd.Timestamp.today()

        try:
            appending = False
            for df in self.get_snapshot_df(reload=True):
                df['update_stamp'] = current_date
                df.to_sql(
                    self.table_name,
                    self.engine,
                    if_exists='append' if appending else 'replace',
                    dtype=self.sql_dtypes)

                appending = True

                print('.', end='', flush=True)

            print()

        except Exception:
            self.logger.exception('Failed to load snapshot')

        else:
            self.logger.info('Loading successful')

    def daily_update_count(self, start_date: str, end_date: str):
        try:

            subquery = f'(SELECT DISTINCT ON (complaint_id) * '
            subquery += f'FROM {self.table_name} '
            subquery += f'WHERE date_sent_to_company IS NULL OR (date_sent_to_company >= \'{start_date}\' AND date_sent_to_company <= \'{end_date}\') '
            subquery += f'ORDER BY complaint_id, update_stamp DESC) t'

            query = f'SELECT date_sent_to_company, company, COUNT(*) '
            query += f'FROM {subquery} '
            query += f'WHERE date_sent_to_company IS NOT NULL '
            query += f'GROUP BY date_sent_to_company, company '
            query += f'ORDER BY date_sent_to_company'
            query += ';'

            daily_update_df = pd.read_sql_query(query, self.engine, index_col='date_sent_to_company')

        except Exception:
            self.logger.exception('Failed to get update count')
        else:
            return daily_update_df

    def get_snapshot_df(self, reload=False):
        '''
        Get table from CFPB.
        reload: if False - up to latest entry in the local db, else get complete table.
        Returns iterable.
        '''

        try:
            limit = self.chunksize
            offset = 0
            rows_read = 0

            # Order by id for consistency
            request_str = self.endpoint + '&$limit={limit}&$offset={offset}&$order=complaint_id'
            if not reload:
                request_str += f'&$where=complaint_id%20<=%20{self.latest_id}'

            while True:
                # Not all fields may be present in json, set columns explicitly
                snapshot_df = pd.DataFrame(columns=self.columns)
                # TODO: Use categorical types
                chunk = pd.read_json(request_str.format(limit=limit, offset=offset))

                if len(chunk) == 0:
                    break

                snapshot_df = snapshot_df.append(chunk, sort=True)

                snapshot_df.set_index('complaint_id', inplace=True)

                yield snapshot_df

                rows_read += len(snapshot_df)

                offset += limit

            self.logger.info(f'{rows_read} rows read')

        except Exception:
            self.logger.exception(f'Failed to load snapshot. Rows read: {rows_read}')

    # OLD
    @property
    def complaint_df(self):
        '''
        Load db content into dataframe.
        Returns iterable.
        '''

        try:
            limit = self.chunksize
            offset = 0

            query = f'SELECT * FROM {self.table_name} ORDER BY complaint_id'
            query += ' LIMIT {limit} OFFSET {offset}'
            query += ';'

            while True:
                complaint_df = pd.DataFrame(columns=self.columns)

                chunk = pd.read_sql_query(
                    query.format(limit=limit, offset=offset),
                    self.engine)

                if len(chunk) == 0:
                        break

                complaint_df = complaint_df.append(chunk, sort=True)

                complaint_df.set_index('complaint_id', inplace=True)

                yield complaint_df

                offset += limit
        except Exception:
            self.logger.exception('Failed to read database')
