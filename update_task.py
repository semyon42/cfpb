#!/bin/env python
from ComplaintDB import ComplaintDB
import os

lock_file = '/tmp/update_complaint_db.lock'


def main():
    comp = ComplaintDB('complaints')

    if(os.path.isfile(lock_file)):
        comp.logger.warning(
            f'Update task is already running. If you are sure it is not, remove {lock_file}')
        return

    with open(lock_file, 'w'):
        pass

    comp.update_db()

    os.remove(lock_file)


if __name__ == '__main__':
    main()
