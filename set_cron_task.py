from crontab import CronTab
import getpass
import os
import sys

os.chdir(sys.path[0])
cmd = os.path.abspath('update_task.py')

cron = CronTab(user=getpass.getuser())

for job in cron:
    if job.comment == 'update_db':
        cron.remove(job)
        cron.write()

job = cron.new(command=cmd, comment='update_db')
job.minute.on(0)
job.hour.every(2)

job.env['PATH'] = os.environ['PATH']

cron.write()
